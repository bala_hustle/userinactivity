import React from 'react';
import { StackNavigator } from 'react-navigation';
import Home from '../src/Home';
import Second from '../src/Second';
import Deck from '../src/SwipeCards/Deck';
import MenuModal from '../src/MenuModal';
import PresentView from '../src/PresentView';

export const HomeStack = StackNavigator({
  Home: {
    screen: Home,
    headerMode: 'none'
  },
  Second: {
    screen: Second
  },
  Deck: {
    screen: Deck
  },
  Menu: {
    screen: MenuModal
  },
  PresentView: {
    screen: PresentView
  }
});
