//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PopupView from './Popup/PopupView';
import { Button } from './common/index';

// create a component
class PresentView extends Component {

    state = {
        popupIsOpen: false
    }

    openPopupView = () =>{
        this.setState({
            popupIsOpen: true
        });
    }

    closePopupView = () => {
        this.setState({
            popupIsOpen: false
        });
    };

    showHidePopup = () => {
        this.setState({
            popupIsOpen: !this.state.popupIsOpen
        });
    };

    render() {
        return (
            <View style={styles.container}>
                <Button onPress={this.showHidePopup}> PresentView </Button>
                <PopupView 
                    isOpen={this.state.popupIsOpen}
                    isClose={this.closePopupView}
                />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ddd',
    },
});

//make this component available to the app
export default PresentView;
