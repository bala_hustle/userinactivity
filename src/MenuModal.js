//import liraries
import React, { Component } from 'react';
import { View, Text, Animated, Easing, StyleSheet } from 'react-native';
import {Button} from './common';
import Fade from './Fade';

// create a component
class MenuModal extends Component {

    constructor(props){
        super(props);
        this.state = {
            menu: false
        }
    }

    componentWillMount(){
       
    }

    showHideView =()=>{
        this.setState({
            menu: !this.state.menu
        })
    }

    showView = () => {
        return(
            <Fade visible={this.state.menu} style={{ flex: 1, }}>
                <View
                    style={[
                        {
                            backgroundColor: '#34495e',
                            height: 200, width: 300,
                            borderRadius: 6,
                            position: 'absolute',
                        },
                    ]}
                />
            </Fade>
        )
    }


    render() {
        
        return (
            <View style={styles.container}>
                <Button onPress={this.showHideView}> Toggle </Button>
                <Fade visible={this.state.menu} style={{position: 'absolute'}}>
                    <View
                        style={[
                            {
                                backgroundColor: '#34495e',
                                height: 200, width: 300,
                                borderRadius: 6,
                            },
                        ]}
                    />
                </Fade>
                <Text> Hello </Text>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ddd',
    },
});

//make this component available to the app
export default MenuModal;
