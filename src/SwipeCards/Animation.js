import React, { Component } from 'react';
import {Text, View, Animated, Easing, TouchableWithoutFeedback } from 'react-native';

class Animation extends Component {
    constructor(props){
        super(props)
        this.state={
            count: 0
        }
    }

    componentWillMount(){
        this.animatedValue = new Animated.Value(1)
        this.position = new Animated.ValueXY(0,0)
        Animated.spring(this.position, {
            toValue: {x: 300, y: 500}
        }).start()
    }

    componentDidMount(){
       
    }

    render(){
        const animatedStyle = {height: this.animatedValue}
        return(
            <View style={styles.container}>
                <Animated.View style={[this.position.getLayout()]}>
                    <View style={styles.circle}/>
                </Animated.View>
            </View>
        )
    }
}

const styles = {
    container: {
        flex: 1,
    },
    box: {
        backgroundColor: '#ddd',
        width: 320,
        height: 100,
        borderRadius: 6,
    },
    circle: {
        marginTop: 0,
        backgroundColor: '#ddd',
        width: 60,
        height: 60,
        borderRadius: 30
    },
    buttonStyle: {
        marginTop: 15,
        alignItems: 'center',
        justifyContent: 'center',
        width: 100,
        height: 50,
        backgroundColor: '#000'
    }
}

export default Animation;