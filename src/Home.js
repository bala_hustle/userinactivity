//import liraries
import React, { Component } from 'react';
import {
  View,
  Text,
  DeviceEventEmitter,
  NativeAppEventEmitter,
  Platform,
  Alert,
  TextInput,
  AppState,
  StyleSheet
} from 'react-native';
import { Button } from './common';
import BackgroundTimer from 'react-native-background-timer';

// var intervalId;
// var inVerify = false;

// create a component
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appState: AppState.currentState,
      time: new Date().toLocaleString(),
      seconds: 5
    };
    // this.handleAppStateChange = this.handleAppStateChange.bind(this);
    // this.tick = this.tick.bind(this);
  }

  // componentWillMount() {
  //   console.disableYellowBox = true;
  // }

  // componentDidMount() {
  //   AppState.addEventListener('change', this.handleAppStateChange);
  // }

  // componentWillUnmount() {
  //   AppState.removeEventListener('change', this.handleAppStateChange);
  // }

  // tick() {
  //   this.setState({
  //     time: new Date().toLocaleString()
  //   });
  // }

  // startTimer() {
  //   intervalId = BackgroundTimer.setInterval(() => {
  //     if (inVerify) {
  //       // this.tick();
  //       // console.log('Background Task is running : ', this.state.time);
  //     }
  //   }, 1000);
  // }

  // stopTimer() {
  //   BackgroundTimer.clearInterval(intervalId);
  // }

  // handleAppStateChange(nextAppState) {
  //   console.log('AppState changed to', nextAppState);

  //   switch (nextAppState) {
  //     case 'active':
  //       if (!inVerify) {
  //         inVerify = true;
  //         this.startTimer();
  //       }
  //       break;

  //     case 'inactive':
  //       var x;
  //       break;

  //     case 'background':
  //       // inVerify = true;
  //       // this.startTimer();
  //       break;

  //     default:
  //       //nothing state or different state, error in app state
  //       break;
  //   }
  // }

  tinderSwipe = er => {
    this.props.navigation.navigate('Second');
  };

  showMenu = () => {
    this.props.navigation.navigate('Menu');
  };

  presentView = () => {
    this.props.navigation.navigate('PresentView');
  };

  render() {
    const { timeWentInactive } = this.state;
    return (
      <View style={styles.container}>
        <Button onPress={this.tinderSwipe}> SwipeCards </Button>
        <Button onPress={this.showMenu}> Show Menu </Button>
        <Button onPress={this.presentView}> Present Modal </Button>
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2c3e50'
  }
});

//make this component available to the app
export default Home;
