//import liraries
import React, { Component } from 'react';
import { View, Text, Animated, Dimensions, TouchableWithoutFeedback, StyleSheet } from 'react-native';

const { width, height } = Dimensions.get('window');

// create a component
class PopupView extends Component {

    state = {
        position: new Animated.Value(this.props.isOpen ? 0 : height),
        visible: this.props.isOpen
    };

    componentWillReceiveProps(nextProps){
        if(!this.props.isOpen && nextProps.isOpen){
            this.animateOpen();
        }

        else if(this.props.isOpen && !nextProps.isOpen){
            this.animateClose();
        }
    }

    animateOpen(){
        this.setState({
            visible: true
        },() =>{
            Animated.timing(this.state.position,{
                toValue: 0,
            }).start()
        });
    }

    animateClose(){
        Animated.timing(this.state.position,{
            toValue: height,
        }).start(()=> this.setState({
            visible: false
        }))
    }

    render() {
        if(!this.state.visible){
            return null;
        }
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.props.isClose}>
                    <Animated.View style={styles.backdrop} />
                </TouchableWithoutFeedback>
                <Animated.View 
                    style={[ styles.modal,
                       { transform: [{ translateY: this.state.position }]}
                    ]}
                >
                <Text> Popup </Text>
                </Animated.View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        backgroundColor: 'transparent',
    },
    backdrop: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'black',
        opacity: 0.2,
    },
    modal: {
        backgroundColor: '#fff',
        height: height/2
    }
});

//make this component available to the app
export default PopupView;
