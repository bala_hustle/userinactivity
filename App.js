import React, { Component } from 'react';
import { Platform, StyleSheet, Alert, Text, View } from 'react-native';
import { HomeStack } from './config/AppNavigation';
import UserInactivity from 'react-native-user-inactivity';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' + 'Shake or press menu button for dev menu'
});

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timeWentInactive: 0
    };
    this.inactiveAction = this.inactiveAction.bind(this);
    this.showAlert = this.showAlert.bind(this);
  }

  componentWillMount() {
    console.disableYellowBox = true;
  }

  showAlert() {
    Alert.alert(
      'InActive Alert',
      'Your session is expired! please login again!',
      [
        {
          text: 'OK',
          onPress: () => {
            console.log('OK Pressed');
          }
        }
      ],
      { cancelable: false }
    );
  }

  onInactivity = timeWentInactive => {
    console.log(timeWentInactive);
    this.setState({
      timeWentInactive
    });
  };

  inactiveAction() {
    const { timeWentInactive } = this.state;
    console.log(timeWentInactive);
    return (
      <View>
        <Text style={{ color: '#000' }}>
          Put your app here
          {timeWentInactive && ` (inactive at: ${timeWentInactive})`}
          {timeWentInactive && this.showAlert()}
        </Text>
      </View>
    );
  }

  render() {
    const { timeWentInactive } = this.state;
    console.log(timeWentInactive);
    return (
      <UserInactivity
        timeForInactivity={30000}
        checkInterval={1000}
        onInactivity={this.onInactivity}
      >
        {this.inactiveAction()}
        <HomeStack />
      </UserInactivity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  }
});
